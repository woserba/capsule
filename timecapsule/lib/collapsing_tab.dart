import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:timecapsule/tab_screen.dart';

class CollapsingTab extends StatefulWidget {
  @override
  _CollapsingTabState createState() => new _CollapsingTabState();
}

class _CollapsingTabState extends State<CollapsingTab> {
  ScrollController scrollController;
  int selectedIndex = 0;

  Widget _buildActions() {
    Widget profile = new GestureDetector(
      onTap: () => showProfile(),
      child: new Container(
        height: 30.0,
        width: 45.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey,
          // image: new DecorationImage(
          //   image: new ExactAssetImage("assets/logo.png"),
          //   fit: BoxFit.cover,
          // ),
          border: Border.all(color: Colors.black, width: 2.0),
        ),
      ),
    );

    double scale;
    if (scrollController.hasClients) {
      scale = scrollController.offset / 300;
      scale = scale * 2;
      if (scale > 1) {
        scale = 1.0;
      }
    } else {
      scale = 0.0;
    }

    return new Transform(
      transform: new Matrix4.identity()..scale(scale, scale),
      alignment: Alignment.center,
      child: profile,
    );
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
    scrollController.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    
    var flexibleSpaceWidget = new SliverAppBar(
      expandedHeight: 125.0,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          title: Text("Capsule Box Id",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              )),
          background: 
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget> [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("BALANCE - IDR",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19.0),textAlign: TextAlign.left,),
                    Text("9.835.000",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19.0),textAlign: TextAlign.left,),
                    Text("1430.54 tp [Time Point]",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19.0),textAlign: TextAlign.left,),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                      Icon(Icons.account_balance_wallet),
                      Text("Wallet",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 19.0),textAlign: TextAlign.left,),
                      ],),
                    Row(
                      children: <Widget>[
                      Icon(Icons.settings_overscan),
                       Text("Scan",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19.0),textAlign: TextAlign.left,),
                      ],)
                  ],
                ),
              ]
            ),    
          // background: Image.asset(
          //   "assets/logo.png",
          // )
          ),
        
      actions: <Widget>[
        new Padding(
          padding: EdgeInsets.all(5.0),
          child: _buildActions(),
        ),
      ],
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Capsule Box Id'),
      ),
      body: 
      new DefaultTabController(
        length: 3,
        child: NestedScrollView(
          controller: scrollController,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              flexibleSpaceWidget,
              SliverPersistentHeader(
                delegate: _SliverAppBarDelegate(
                  TabBar(
                    labelColor: Colors.black87,
                    unselectedLabelColor: Colors.black26,
                    tabs: [
                      Tab(
                        icon: Icon(Icons.account_box),
                        text: "NAP",
                      ),
                      Tab(icon: Icon(Icons.add_location), text: "STAY"),
                      Tab(icon: Icon(Icons.monetization_on), text: "RENT"),
                    ],
                  ),
                ),
                pinned: true,
              ),
            ];
          },
          body: new TabBarView(
            children: <Widget>[
              new TabScreen("NAP"),
              new TabScreen("STAY"),
              new TabScreen("RENT"),
            ],
          ),
        ),
      ),
       floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey[700],
        child: const Icon(Icons.add),
        onPressed: () {},
      ),
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.grey[700],
         
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
          BottomNavigationBarItem(icon: Icon(Icons.list), title: Text('Orders')),
          BottomNavigationBarItem(
              icon: Icon(Icons.mail), title: Text('Inbox')),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text('Account')),
              
        ],
        currentIndex: selectedIndex,
        fixedColor: Colors.white,
        onTap: onItemTapped,
      ),
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  showProfile() {
    Navigator.pushNamed(context, '/profile');
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final TabBar _tabBar;

  _SliverAppBarDelegate(this._tabBar);

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}