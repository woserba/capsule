import 'package:flutter/material.dart';
import 'package:timecapsule/collapsing_profile.dart';
import 'package:timecapsule/collapsing_tab.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Capsule Box',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.grey,
      ),  
      initialRoute: '/',
      routes: {
        // When we navigate to the "/" route, build the FirstScreen Widget
        '/': (context) => CollapsingTab(),
        // When we navigate to the "/profile" route, build the SecondScreen Widget
        '/profile': (context) => CollapsingProfile(),
      },
    );
  }
}








      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.grey[700],
      //   child: const Icon(Icons.add),
      //   onPressed: () {},
      // ),
      // bottomNavigationBar: BottomNavigationBar(
      //     type: BottomNavigationBarType.fixed,
      //   backgroundColor: Colors.grey[700],
         
      //   items: <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
      //     BottomNavigationBarItem(icon: Icon(Icons.list), title: Text('Orders')),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.mail), title: Text('Inbox')),
      //     BottomNavigationBarItem(
      //         icon: Icon(Icons.person), title: Text('Account')),
              
      //   ],
      //   currentIndex: selectedIndex,
      //   fixedColor: Colors.white,
      //   onTap: onItemTapped,
      // ),

  //       int selectedIndex = 0;
  // final widgetOptions = [
  //   Text('Home'),
  //   Text('Orders'),
  //   Text('Inbox'),
  //   Text('Account'),
  // ];


  // void onItemTapped(int index) {
  //   setState(() {
  //     selectedIndex = index;
  //   });
  // }